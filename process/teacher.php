<?php
require_once '../config/init.php';
require_once '../inc/checkLogin.php';
$teacher = new Teacher;
$user = new User;
if (isset($_POST) && !empty($_POST) && isset($_POST['teacher_name'], $_POST['teacher_class_id'],$_POST['teacher_subject_id'], $_POST['status']) && !empty($_POST['teacher_name']) && !empty($_POST['teacher_class_id']) && !empty($_POST['teacher_subject_id']) && !empty($_POST['status'])) {
    // debug($_FILES['teacher_image']);
    // debug($_POST);
    $act = "add";
    if (isset($_POST['user_id'], $_POST['teacher_id']) && !empty($_POST['user_id']) && !empty($_POST['teacher_id'])) {
        $act = "updat";
    }
    if ($act == "add") {
        if (!filter_var($_POST['teacher_email'], FILTER_VALIDATE_EMAIL)) {
            redirect("../teachers.php", "error", "Email format does not match!");
        }
        if ($_POST['teacher_password'] != $_POST['teacher_repassword']) {
            redirect("../teachers.php", "error", "Password and confirm password does not match!");
        }
        $email = $_POST['teacher_email'];
        $password = sha1($_POST['teacher_email'] . $_POST['teacher_password']);
    }

    $userdata = array(
        'name' => sanitize($_POST['teacher_name']),
        'status' => sanitize($_POST['status']),
        'added_by' => $_SESSION['user_id'],
        'role' => 'teacher',
    );
    if (isset($_FILES['teacher_image']) && $_FILES['teacher_image']['error'] == 0) {
        $teacher_image = imageUpload($_FILES['teacher_image'], "teacher");
        if ($teacher_image) {
            $userdata['image'] = $teacher_image;
        }
        if ($act != "add") {
            $user_info = $user->getRowByRowId($_POST['user_id']);
            deleteImage($user_info[0]->image, "teacher");
        }
    }
    if ($act == "add") {
        $userdata['email'] = $email;
        $userdata['password'] = $password;
        $user_id = $user->insertData($userdata);
    } else {
        $user_id = $user->updateData($userdata, $_POST['user_id']);
    }
    // $user_id = 50;
    if ($user_id) {
        $teacher_data = array(
            'user_id' => $user_id,
            'class_id' => $_POST['teacher_class_id'],
            'section_id' => $_POST['teacher_section_id'],
            'subject_id' => $_POST['teacher_subject_id'],
            'education' => $_POST['teacher_education'],
            'experience' => $_POST['teacher_experience'],
            'added_by' => $_SESSION['user_id'],
        );
        // debug($teacher_data);
        if($_POST['teacher_section_id'] == ''){
            unset($teacher_data['section_id']);
        }
        // debug($teacher_data, true);
        if ($act == "add") {
            $teacher_id = $teacher->insertData($teacher_data);
        } else {
            $teacher_id = $teacher->updateData($teacher_data, $_POST['teacher_id']);
        }
        if ($teacher_id) {
            redirect('../teachers.php', 'success', 'Teacher ' . $act . 'ed successfully');
        } else {
            redirect('../teachers.php', 'error', 'Sorry!, error while ' . $act . 'ing teacher.');
        }

    } else {
        redirect('../teachers.php', 'error', 'Sorry!, error while creating user.');
    }

} elseif (isset($_GET, $_GET['id']) && !empty($_GET['id'])) {
    $id = (int) $_GET['id'];
    if ($id <= 0) {
        redirect('../teachers.php', 'error', 'Invalid teacher id.');
    }
    $teacher_info = $teacher->getRowByRowId($id);
    if (!$teacher_info) {
        redirect('../teachers.php', 'error', 'Teacher does not exist.');
    }
    $user_info = $user->getRowByRowId($teacher_info[0]->user_id);
    $status = $teacher->deleteRowByRowId($id);
    if ($status) {
        //    debug($teacher_info,true);
        $del = $user->deleteRowbyRowId($teacher_info[0]->user_id);
        if ($del) {
            deleteImage($user_info[0]->image, "teacher");
        }
        redirect('../teachers.php', 'success', 'Teacher deleted successfully.');
    } else {
        redirect('../teachers.php', 'error', 'Sorry!, Error while deleting this teacher.');
    }
} else {
    redirect('../teachers.php', 'error', 'Add some data.');
}
