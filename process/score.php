<?php
require_once '../config/init.php';
require_once '../inc/checkLogin.php';
$schedule = new Schedule;
if (isset($_POST) && !empty($_POST)) {
    $exam_id = (int) $_POST['exam_id'];
    $student_id = (int) $_POST['student_id'];
    if($exam_id <= 0 || $student_id <=0){
        redirect('../students.php', 'error', 'Select exam first for score update.');

    }
    $act = "add";
    if(isset($_POST['result_id']) &&  !empty($_POST['result_id'])){
        $result_id = $_POST['result_id'][0];
        // debug($result_id);
        $act = "updat";
    }
    $over_all_result = "pass";
    $ob_total = 0;
    $full_total = 0;
    if($_POST['score']){
        foreach ($_POST['score'] as $sub => $ob_score) {
            $sub_result = "pass";
            $schedule_info = $schedule->getSubjectByExamId($exam_id,$sub);
            $ob_total += $ob_score;
            $full_total += $schedule_info[0]->full_marks;
            if($schedule_info[0]->pass_marks > $ob_score){
                $sub_result = "fail";
                $over_all_result = "fail";
            }
            $scor = new Score;
            $score_id = $scor->getScoreId($exam_id,$student_id,$sub);
            // debug($score_id);
            $score_data = array(
                'exam_id' => $exam_id,
                'student_id' => $student_id,
                'subject_id' => $sub,
                'obtained_score' => $ob_score,
                'added_by' => $_SESSION['user_id'],
                'status' => $sub_result
            );
            if($act == 'updat'){
                unset($score_data['exam_id']);
                unset($score_data['student_id']);
                unset($score_data['subject_id']);
                unset($score_data['added_by']);
                $status1 = $scor->updateData($score_data,$score_id[0]->score_id);
            }else{
                $status1 = $scor->insertData($score_data);
            }
            // debug($score_data);
        }
        // debug($_POST['score'], true);
        $percentage = ($ob_total/$full_total)*100;
        $result_data = array(
                'exam_id' => $exam_id,
                'student_id' => $student_id,
                'total_obtained_score' => $ob_total,
                'percentage' => number_format($percentage,3),
                'result' => $over_all_result
        );
        // echo $result_id." this is result is";
        // debug($result_data, true);
        $result = new Result;
        if($status1){
            if ($act == 'updat') {
                unset($result_data['exam_id']);
                unset($result_data['student_id']);
                $status = $result->updateData($result_data, $result_id);
            }else{
                $status = $result->insertData($result_data);
            }
            if ($status) {
                redirect('../students.php', 'success', 'Score ' . $act . 'ed successfully');
            }
        }else{
            redirect('../students.php', 'error', 'Soryy!, error while '.$act.'ing successfully'); 
        }
    }
} else {
    redirect('../students.php', 'error', 'Score add first');
}
