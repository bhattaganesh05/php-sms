<?php
require_once "../config/init.php";
require_once "../inc/checkLogin.php";
$class = new Classes;
$section = new Section;
$subject = new Subject;
$schedule = new Schedule;
$score = new Score;
$result = new Result;
$act =  (isset($_REQUEST['act'])) ? sanitize($_REQUEST['act']) : null;
if($act ==  'get.class'){
	// debug($_REQUEST);
	$class_id = (int)$_REQUEST['class_id'];
	if($class_id <= 0){
		api_response(null,false,'Invalid class id.');
	}
	$class_info = $class->getRowByRowId($class_id);
	if(!$class_info){
		api_response(null,false,'Class not found.');
	}
	api_response($class_info,true,'success');
}elseif($act ==  'get-sections'){
	$class_id = (int) $_REQUEST['class_id'];
	if ($class_id <= 0) {
		api_response(null, false, 'Invalid class id.');
	}
	$class_section = $section->getSectionByClass($class_id);
	if (!$class_section) {
		api_response(null, false, 'Section not found.');
	}
	api_response($class_section, true, 'success');

}elseif($act == 'get-sections-with-subject'){
$class_id = (int) $_REQUEST['class_id'];
if ($class_id <= 0) {
    api_response(null, false, 'Invalid class id.');
}
$class_section = $section->getSectionByClass($class_id);
$all_subjects = $subject->getSubjectByClass($class_id);
$data = array(
	'section_data' => $class_section,
	'all_subjects' =>  $all_subjects
);
api_response($data,true,'success');
}elseif($act == "get_exam-schedule"){
	$exam_id = (int) $_REQUEST['exam_id'];
if ($exam_id <= 0) {
    api_response(null, false, 'Invalid exam id.');
}
$schedules = $schedule->getSchueduleByExamId($exam_id);
if (!$schedules) {
    api_response(null, false, 'No Schedule found for this exam.');
}
api_response($schedules, true, 'success');

}elseif($act == "get_exam_scedule_and_score"){
	$exam_id = (int) $_REQUEST['exam_id'];
	$student_id = (int) $_REQUEST['student_id'];
	if ($exam_id <= 0 || $student_id <= 0) {
		api_response(null, false, 'Invalid exam or student id.');
	}
	$schedules = $schedule->getSchueduleByExamId($exam_id);
	if(!$schedules){
		api_response(null, false, 'No Schedule found for this exam.');
	}else{
		$scores = $score->getSchueduleAndScoreByExamId($exam_id,$student_id);
		$results = $result->getResultIdByExamId($exam_id,$student_id);
		if (!$scores) {
			$full_data[] = $schedules;
		api_response($full_data, true, 'success');
		}else{
			$full_data [] = $schedules;
			$full_data[] = $scores;
			$full_data [] = $results;
			api_response($full_data, true, 'success');
		}
	}
}else{
	api_response(null,false,'Action not specified.');
}