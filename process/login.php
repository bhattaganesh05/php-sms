<?php
require_once "../config/init.php";
$user = new User();
if(isset($_POST,$_POST['email'],$_POST['password'])){
    //form submitted
    $email = filter_var($_POST['email'],FILTER_VALIDATE_EMAIL);
    if(!$email){ //if invalid email value, null
        redirect('../','error',"Email format is not valid.");
    }
    $password = sha1($email.$_POST['password']);
    // debug($_POST);
    $user_info =  $user->getUserByEmail($email);
    // debug($user_info);
    if($user_info){
        // debug($user_info);
        // echo sha1($email.'admin1234');
        if($password == $user_info[0]->password){
            // debug($user_info);
        }else{
            redirect("../","error","Password does not match.");
        }
        if($user_info[0]->status == 'active'){
            debug($user_info);
            setSession('user_id',$user_info[0]->id);
            setSession('user_name',$user_info[0]->name);
            setSession('user_email',$user_info[0]->email);
            setSession('user_role',$user_info[0]->role);
            setSession('user_image',$user_info[0]->image);
            $token = randomString();
            setSession('token',$token);
            if(isset($_POST['remember_me']) && !empty($_POST['remember_me'])){
                setCookie('_au',$token,time()+864000,"/");
                $data = array(
                    'remember_token' => $token
                );
                $user->updateData($data,$user_info[0]->id);
            }
            if($user_info[0]->role == "admin"){
                redirect("../dashboard.php","success","Welcome to admin panel.");
            }else if($user_info[0]->role == "teacher"){
                redirect("../teacherDashboard.php","success","Welcome to Teacher panel.");
            }{
                redirect("../studentDashboard.php","success","Welcome to student panel.");
            }
        }else{
            redirect("../","error","Your account has been disabled. Please contact system
             administration");
        }
    }else{
        redirect("../","error","User does not exist.");
    }
}else{
    redirect("../","error","Please login first.");
}
