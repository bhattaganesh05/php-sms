<?php
require_once '../config/init.php';
require_once '../inc/checkLogin.php';
$user = new User;
if(isset($_POST) && !empty($_POST)){
    debug($_POST);
    debug($_FILES);
    $data = array(
    	'name' => sanitize($_POST['name'])
    );
    $file_upload = imageUpload($_FILES['image'],"user");
    if($file_upload){
    	$data['image'] = $file_upload;
    }
    $update_status = $user->updateData($data,$_SESSION['user_id']);
    if($update_status){
    	setSession('user_name',$data['name']);
    	if(isset($file_upload)){
        if($_SESSION['user_image'] != null){
          deleteImage($_SESSION['user_image'],"user");
        }
    		setSession('user_image',@$data['image']);
    	}
		redirect('../dashboard.php','success','User updated successfully.');


    }else{
		redirect('../','error','Sorry! , Error while updating user.');
    }
}else{
	redirect('../','error','Update user data  first.');
}

