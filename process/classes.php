<?php
require_once '../config/init.php';
require_once '../inc/checkLogin.php';
$classes =  new Classes;
if(isset($_POST) && !empty($_POST) && isset($_POST['class_name']) &&  !empty($_POST['class_name'])){
    debug($_POST);
    $data = array(
    	'class_name' => sanitize($_POST['class_name']),
        'class_detail' => sanitize($_POST['class_detail']),
        'class_status' => sanitize($_POST['class_status']),
        'added_by' => $_SESSION['user_id']
    );
    $class_id = !empty($_POST['class_id']) ? (int) $_POST['class_id'] :null;
    if($class_id){
        $act = "updat";
        $class_id = $classes->updateData($data,$class_id);
    }else{
        $act = "add";
        $class_id = $classes->insertData($data);
    }
    if($class_id){
        redirect('../classes.php','success','Class '.$act.'ed successfully');
    }else{
        redirect('../classes.php','error','Sorry!, error while '.$act.'ing class.');
    }
}elseif (isset($_GET,$_GET['id']) && !empty($_GET['id'])) {
    $id = (int)$_GET['id'];
    if($id<=0){
        redirect('../classes.php','error','Invalid class id.');
    }
    $class_info =  $classes->getRowByRowId($id);
    if(!$class_info){
        redirect('../classes.php','error','Class does not exist.');
    }
    $status = $classes->deleteRowByRowId($id);
    if($status){
      redirect('../classes.php','success','Class deleted successfully.');
    }else{
      redirect('../classes.php','error','Sorry!, Error while deleting this class.');
    }
}else{
	redirect('../classes.php','error','Update user data  first.');
}

