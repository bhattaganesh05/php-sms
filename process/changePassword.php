<?php
require_once '../config/init.php';
require_once '../inc/checkLogin.php';
$user = new User;
if(isset($_POST) && !empty($_POST) && !empty($_POST['pwd']) && !empty($_POST['cpwd'])){
    // debug($_POST, true);
    if($_POST['pwd'] != $_POST['cpwd']){
        redirect('../dashboard.php','error','Sorry! , Confirm password does not match.');
    }
    $data = array(
        'password' => sha1($_SESSION['user_email'].$_POST['pwd'])
    );
    $update_status = $user->updateData($data,$_SESSION['user_id']);
    if($update_status){
		redirect('../logout.php');

    }else{
		redirect('../','error','Sorry! , Error while changing password.');
    }
}else{
	redirect('../','error','Update user data  first.');
}

