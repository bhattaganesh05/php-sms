<?php
require_once '../config/init.php';
require_once '../inc/checkLogin.php';
$exam = new Exam;
$schedule = new Schedule;//not recommended
if(isset($_POST) && !empty($_POST)){
    // debug($_POST,true);
    $exam_data = array(
        'title' => $_POST['exam_title'],
        'start_date' => $_POST['exam_start_date'],
        'end_date' => $_POST['exam_end_date'],
        'status' => $_POST['status'],
        'added_by' => $_SESSION['user_id']
    );

    if(isset($_POST['section_id']) && !empty($_POST['section_id'])){
        $exam_data['section_id'] =(int) $_POST['section_id'];
    }
    if(isset($_POST['exam_class_id']) && !empty($_POST['exam_class_id'])){
        $exam_data['class_id'] =(int) $_POST['exam_class_id'];
    }
    // debug($exam_data);
    $exam_id = (isset($_POST['exam_id']) && !empty($_POST['exam_id'])) ? $_POST['exam_id'] : '';
    if ($exam_id) {
        $act = "updat";
        unset($exam_data['added_by']);
        $exam_id = $exam->updateData($exam_data,$exam_id);
    } else {
        $act = "add";
        $exam_id = $exam->insertData($exam_data);
    }


    // debug($exam_id, true);
    if(isset($_POST['exam_schedule']) && !empty($_POST['exam_schedule'])){
        foreach ($_POST['exam_schedule'] as $sub_id => $exam_schedule) {
            $exam_schedule_data =  array(
                'exam_id' => $exam_id,
                'subject_id' => $sub_id,
                'exam_date' => $exam_schedule['date'],
                'full_marks' => $exam_schedule['full_marks'],
                'pass_marks' => $exam_schedule['pass_marks']
            );
            // $schedule = new Schedule;
            $old_exam = $schedule->checkScheduleExists($exam_id,$sub_id);
            if($old_exam){
                $schedule_id = $schedule->updateData($exam_schedule_data,$old_exam[0]->id);
            }else{
                $schedule_id = $schedule->insertData($exam_schedule_data);
            }
        }
    }
    if($exam_id){
        redirect('../exams.php', 'success', 'Exam '.$act.'ed successfully.');
    }else{
        redirect('../exams.php', 'error', 'Sorry! error while '.$act.'ing exam');
    }

}elseif (isset($_GET, $_GET['id']) && !empty($_GET['id'])) {
    $id = (int) $_GET['id'];
        // $id = 4;
        //    debug($_GET,true);
    if ($id <= 0) {
        redirect('../exams.php', 'error', 'Invalid exam id.');
    }
    $exam_info = $exam->getRowByRowId($id);
    if (!$exam_info) {
        redirect('../exams.php', 'error', 'Exam does not exist.');
    }
    $status = $exam->deleteRowByRowId($id);
    if($status){
        //    debug($exam_info,true);
        redirect('../exams.php', 'success', 'Exam deleted successfully.');
    } else {
        redirect('../exams.php', 'error', 'Sorry!, Error while deleting this exam.');
    }
}else{
    redirect('../exams.php','error','Add exam data'); 
}