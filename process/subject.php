<?php
require_once '../config/init.php';
require_once '../inc/checkLogin.php';
$subject =  new Subject;
if(isset($_POST) && !empty($_POST) && isset($_POST['subject_name']) &&  !empty($_POST['subject_name'])){
    // debug($_POST,true);
    $data = array(
        'subject_name' => sanitize($_POST['subject_name']),
        'subject_code' => sanitize($_POST['subject_code']),
        'subject_desc' => sanitize($_POST['subject_Description']),
        'status' => sanitize($_POST['status']),
        'class_id' => sanitize($_POST['class_id']),
    	'teacher_id' => sanitize($_POST['teacher_id']),
        'added_by' => $_SESSION['user_id']
    );
    // debug($data,true);
    $subject_id = !empty($_POST['subject_id']) ? (int) $_POST['subject_id'] :null;
    if($subject_id){
        $act = "updat";
        unset($data['subject_code']);
        $subject_id = $subject->updateData($data,$subject_id);
    }else{
        $act = "add";
        $subject_id = $subject->insertData($data);
    }
    if($subject_id){
        redirect('../subject.php','success','Subject '.$act.'ed successfully');
    }else{
        redirect('../subject.php','error','Sorry!, error while '.$act.'ing subject.');
    }
}elseif (isset($_GET,$_GET['id']) && !empty($_GET['id'])) {
    $id = (int)$_GET['id'];
    if($id<=0){
        redirect('../subject.php','error','Invalid subject id.');
    }
    $subject_info =  $subject->getRowByRowId($id);
    if(!$subject_info){
        redirect('../subject.php','error','Subject does not exist.');
    }
    $status = $subject->deleteRowByRowId($id);
    if($status){
      redirect('../subject.php','success','Subject deleted successfully.');
    }else{
      redirect('../subject.php','error','Sorry!, Error while deleting this subject.');
    }
}else{
	redirect('../subject.php','error','Update user data  first.');
}

