<?php
require_once '../config/init.php';
require_once '../inc/checkLogin.php';
$student = new Student;
$user = new User;
if (isset($_POST) && !empty($_POST) && isset($_POST['student_name'],$_POST['class_id'],$_POST['status']) && !empty($_POST['student_name']) &&  !empty($_POST['class_id']) && !empty($_POST['status'])) {
    // debug($_FILES['student_image']);
    // debug($_POST,true);
    $act = "add";
    if(isset($_POST['user_id'],$_POST['student_id']) && !empty($_POST['user_id']) && !empty($_POST['student_id'])){
        $act = "updat";
    }
    if($act == "add"){
        if(!filter_var($_POST['student_email'],FILTER_VALIDATE_EMAIL)){
            redirect("../students.php","error","Email format does not match!");
        }
        if ($_POST['student_password'] != $_POST['student_repassword']) {
            redirect("../students.php", "error", "Password and confirm password does not match!");
        }
        $email = $_POST['student_email'];
        $password = sha1($_POST['student_email'].$_POST['student_password']);
    }

    $userdata = array(
        'name' => sanitize($_POST['student_name']),
        'status' => sanitize($_POST['status']),
        'added_by' => $_SESSION['user_id'],
        'role' => 'student'
    );
    if(isset($_FILES['student_image']) &&  $_FILES['student_image']['error'] == 0){
        $student_image = imageUpload($_FILES['student_image'],"student");
        if($student_image){
            $userdata['image'] = $student_image;
        }
        if($act != "add"){
            $user_info = $user->getRowByRowId($_POST['user_id']);
            deleteImage($user_info[0]->image,"student");
        }
    }
    if($act == "add"){
        $userdata['email'] = $email;
        $userdata['password'] = $password;
        $user_id = $user->insertData($userdata);
    }else{
        $user_id = $user->updateData($userdata,$_POST['user_id']);
    }
    // $user_id = 50;
    if($user_id){
        $student_data = array(
            'user_id' => $user_id,
            'class_id' => (int) $_POST['class_id'],
            'added_by' => $_SESSION['user_id'],
            'roll_no' => $student->getRollno($_POST['class_id'],$_POST['section_id'])
        );
        if (isset($_POST['section_id']) && !empty($_POST['section_id'])) {
            $student_data['section_id'] = (int) $_POST['section_id'];
        }

        // debug($student_data, true);
        if($act == "add"){
            $student_id = $student->insertData($student_data);
        }else{
            $student_id = $student->updateData($student_data,$_POST['student_id']);
        }
        if ($student_id) {
            redirect('../students.php', 'success', 'Student ' . $act . 'ed successfully');
        } else {
            redirect('../students.php', 'error', 'Sorry!, error while ' . $act . 'ing student.');
        }

    }else{
        redirect('../students.php', 'error', 'Sorry!, error while creating user.');
    }

} elseif (isset($_GET, $_GET['id']) && !empty($_GET['id'])) {
    $id = (int) $_GET['id'];
    if ($id <= 0) {
        redirect('../students.php', 'error', 'Invalid student id.');
    }
    $student_info = $student->getRowByRowId($id);
    if (!$student_info) {
        redirect('../students.php', 'error', 'Student does not exist.');
    }
    $user_info = $user->getRowByRowId($student_info[0]->user_id);
    $status = $student->deleteRowByRowId($id);
    if($status){
        //    debug($student_info,true);
        $del = $user->deleteRowbyRowId($student_info[0]->user_id);
        if($del){
            deleteImage($user_info[0]->image,"student");
        }
        redirect('../students.php', 'success', 'Student deleted successfully.');
    } else {
        redirect('../students.php', 'error', 'Sorry!, Error while deleting this student.');
    }
} else {
    redirect('../students.php', 'error', 'Add some data.');
}
