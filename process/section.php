<?php
require_once '../config/init.php';
require_once '../inc/checkLogin.php';
$section =  new Section;
if(isset($_POST) && !empty($_POST) && isset($_POST['section_name']) &&  !empty($_POST['section_name'])){
    // debug($_POST,true);
    $data = array(
        'section_name' => sanitize($_POST['section_name']),
    	'class_id' => sanitize($_POST['section_class_id']),
        'added_by' => $_SESSION['user_id']
    );
    $section_id = !empty($_POST['section_id']) ? (int) $_POST['section_id'] :null;
    if($section_id){
        $act = "updat";
        $section_id = $section->updateData($data,$section_id);
    }else{
        $act = "add";
        $section_id = $section->insertData($data);
    }
    if($section_id){
        redirect('../classes.php','success','Section '.$act.'ed successfully');
    }else{
        redirect('../classes.php','error','Sorry!, error while '.$act.'ing section.');
    }
}elseif (isset($_GET,$_GET['id']) && !empty($_GET['id'])) {
    $id = (int)$_GET['id'];
    if($id<=0){
        redirect('../classes.php','error','Invalid section id.');
    }
    $section_info =  $section->getRowByRowId($id);
    if(!$section_info){
        redirect('../classes.php','error','Section does not exist.');
    }
    $status = $section->deleteRowByRowId($id);
    if($status){
      redirect('../classes.php','success','Section deleted successfully.');
    }else{
      redirect('../classes.php','error','Sorry!, Error while deleting this section.');
    }
}else{
	redirect('../classes.php','error','Update user data  first.');
}

