<?php
$user =  new User;
if(!isset($_SESSION,$_SESSION['token']) || empty($_SESSION['token'])){
    if(isset($_COOKIE,$_COOKIE['_au'])){
        $cookie_data = $_COOKIE['_au'];
        $user_info = $user->getUserByCookieToken($cookie_data);
        if($user_info){
            setSession('user_id',$user_info[0]->id);
            setSession('user_name',$user_info[0]->name);
            setSession('user_email',$user_info[0]->email);
            setSession('user_role',$user_info[0]->role);
            setSession('user_image',$user_info[0]->image);
            $token = randomString();
            setSession('token',$token);
            setCookie('_au',$token,time()+864000,"/");
            $data = array(
                'remember_token' => $token
            );
            $user->updateData($data,$user_info[0]->id);
        }else{
            setcookie('_au','',time()-60,'/');
            redirect('./','warning','Clear your browser cookie before login.');
        }
    }else{
        redirect('./','error','Please Login first.');
    }
}