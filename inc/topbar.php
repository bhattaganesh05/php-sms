<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
  <!-- Sidebar Toggle (Topbar) -->
  <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
  <i class="fa fa-bars"></i>
  </button>
  <!-- Topbar Search -->
  <!-- <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
    <div class="input-group">
      <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
      <div class="input-group-append">
        <button class="btn btn-primary" type="button">
        <i class="fas fa-search fa-sm"></i>
        </button>
      </div>
    </div>
  </form> -->
  <!-- Topbar Navbar -->
  <ul class="navbar-nav ml-auto">
    <!-- <div class="topbar-divider d-none d-sm-block"></div> -->
    <!-- Nav Item - User Information -->
    <li class="nav-item dropdown no-arrow">
      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $_SESSION['user_name']; ?></span>
        <?php
        if(isset($_SESSION,$_SESSION['user_image']) && !empty($_SESSION['user_image']) && file_exists(UPLOAD_DIR.'/user/'.$_SESSION['user_image'])){
        $image_url = UPLOAD_URL."/user/".$_SESSION['user_image'];
        }else{
        $image_url = IMAGES_URL.'/admin.jpg';
        }
        ?>
        <img class="img-profile rounded-circle" src="<?php echo $image_url; ?>">
      </a>
      <!-- Dropdown - User Information -->
      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
        <a class="dropdown-item"  id = "profileUpdate">
          <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
          Profile
        </a>
        <a class="dropdown-item" id="changePassword">
          <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
          Change Password
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
          Logout
        </a>
      </div>
    </li>
  </ul>
</nav>
<!-- profile Modal-->
<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Profile Update</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <form action="process/profile.php" method = "post" enctype = "multipart/form-data">
        <div class="modal-body">
          <div class="form-group row">
            <div class="col-3">
              <label class="form-control-label" for="name">User Name:</label>
            </div>
            
            <div class="col-9">
              <input type="text" class="form-control form-control-sm" id="name" name="name" value="<?php echo $_SESSION['user_name'] ?>">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-3">
              <label class="form-control-label text-center" for="image">User Image:</label>
            </div>
            
            <div class="col-4">
              <input type="file" class="form-control-file" name="image" id="image" accept = "image/*" onchange="readURL(this,'thumb')">
            </div>
            <div class="col-5">
              <img src="<?php echo $image_url ?>" id = 'thumb' alt="<?php echo $_SESSION['user_name'] ?>" class ="rounded-circle" width ="100" height = "100" >
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger" type="button" data-dismiss="modal"><i class = "fa fa-times"></i> Cancel</button>
          <button type= "submit" class="btn btn-success"><i class = "fa fa-paper-plane"></i> Save Changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- change password Modal-->
<div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <form action="process/changePassword.php" method = "post">
        <div class="modal-body">
          <div class="form-group row">
            <div class="col-3">
              <label class="form-control-label" for="pwd">Password:</label>
            </div>
            
            <div class="col-9">
              <input type="password" class="form-control form-control-sm" id="pwd" name="pwd">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-3">
              <label class="form-control-label text-center" for="cpwd">Confirm Password:</label>
            </div>
            
            <div class="col-9">
              <input type="password" class="form-control form-control-sm" name="cpwd" id="cpwd">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger" type="button" data-dismiss="modal"><i class = "fa fa-times"></i> Cancel</button>
          <button type= "submit" class="btn btn-success"><i class = "fa fa-paper-plane"></i> Save Changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- End of Topbar -->