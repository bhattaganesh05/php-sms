<?php
require_once "config/init.php";
require_once "inc/checkLogin.php";
$_title = "Students List || " . SITE_TITLE;
require_once "inc/header.php";
$student = new Student;
?>
<div id="wrapper">
  <?php require_once 'inc/sidebar.php';?>
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
      <?php require_once 'inc/topbar.php';?>
      <!-- Begin Page Content -->
      <div class="container-fluid">
        <?php flash();?>
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800 font-weight-bold">Students List
        <a href="student-form.php"  class=" addModal btn btn-success btn-sm float-right"> <i class="fa fa-plus"></i> Add Student</a>
        </h1>
        <hr>
        <div class="row">
          <div class="col-12">
            <table class="table table-bordered table-hover table-sm text-center">
              <thead class="thead-dark">
                <th>Name</th>
                <th>Class</th>
                <th>Section</th>
                <th>Roll No.</th>
                <th>Status</th>
                <th>Action</th>
              </thead>
              <tbody>
                <?php

$all_data = $student->getAllStudnetInfo();
// debug($all_data,true);
if ($all_data) {
    foreach ($all_data as $key => $row) {
        ?>
                <tr>
                  <td><?php echo $row->user_name ?></td>
                  <td><?php echo $row->class_name ?></td>
                  <td><?php echo $row->section_name ?></td>
                  <td><?php echo $row->roll_no ?></td>
                  <td>
                    <span class = "badge badge-<?php echo ($row->status == 'active') ? 'success' : 'danger'; ?>">
                      <?php echo ucfirst($row->status) ?>
                    </span>
                  </td>
                  <td>
                    <a href="student-form.php?id=<?php echo $row->id ?>" title ="Edit class" class="btn btn-sm btn-primary btn-circle editClass" data-id = "<?php echo $row->id ?>">
                      <i class="fa fa-pen"></i>
                    </a>
                    <a href="process/student.php?id=<?php echo $row->id ?>" onclick="return confirm('Are you sure you want to delete this student?')" title ="Delete class" class="btn btn-sm btn-danger btn-circle">
                      <i class="fa fa-trash"></i>
                    </a>
                    <a href="./score_add.php?id=<?php echo $row->id ?>" title = "Add score" class = "btn btn-info btn-circle btn-sm">
                      <i class="fa fa-plus"></i>
                    </a>
                  </td>
                </tr>
                <?php
}
}
?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
    <?php require_once 'inc/copyRight.php';?>
  </div>
  <!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>
<?php require_once 'inc/footer.php';?>
