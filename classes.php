<?php
require_once "config/init.php";
require_once "inc/checkLogin.php";
$_title = "Classes List || ".SITE_TITLE;
require_once "inc/header.php";
?>
<div id="wrapper">
  <?php require_once 'inc/sidebar.php'; ?>
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
      <?php require_once 'inc/topbar.php'; ?>
      <!-- Begin Page Content -->
      <div class="container-fluid">
        <?php flash(); ?>
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800 font-weight-bold">Classes List
        <a href="javascript:;"  class=" addModal btn btn-success btn-sm float-right"> <i class="fa fa-plus"></i> Add Class</a>
        </h1>
        <hr>
        <div class="row">
          <div class="col-12">
            <table class="table table-bordered table-hover table-sm text-center">
              <thead class="thead-dark">
                <th>Title</th>
                <th>Description</th>
                <th>Section</th>
                <th>Status</th>
                <!-- <th>Added By</th> -->
                <th>Action</th>
              </thead>
              <tbody>
                <?php
                $classes = new Classes;
                $all_data = $classes->selectAllRows();
                if($all_data){
                foreach ($all_data as $key => $row) {
                ?>
                <tr>
                  <td><?php echo $row->class_name ?></td>
                  <td><?php echo $row->class_detail ?></td>
                  <td>
                    <?php
                    $section = new Section;
                    $get_sections = $section->getSectionByClass($row->id);
                    if($get_sections){
                    ?>
                    <span class = "btn btn-link  text-info font-weight-bold show-section">View Section</span>
                    <ul class = 'section-list collapse'>
                      <?php
                      // $tmp = array();
                      foreach ($get_sections as $section => $section_info) {
                      echo"<li class = 'section-name'>".$section_info->section_name;
                        ?>
                        <a href="javascript:;" title ="Edit Section" class="btn btn-xs btn-primary btn-circle  ml-3 edit_section" data-class_id = "<?php echo $section_info->class_id ?>" data-data = '{"id" : <?php echo $section_info->id?>,"section_name" : "<?php echo $section_info->section_name ?>"}'>
                          <i class="fa fa-pen"></i>
                        </a>
                        <a href="process/section.php?id=<?php echo $section_info->id ?>" title ="Delete section" class="btn btn-xs btn-danger btn-circle delete_section" onclick="return confirm('Are you sure you want to delete this section?')">
                          <i class="fa fa-times"></i>
                        </a>
                      </li>
                      <?php
                      }
                      // echo implode(', ',$tmp);
                      }
                      ?>
                    </ul>
                  </td>
                  <td>
                    <span class = "badge badge-<?php echo($row->class_status == 'active')?'success':'danger';?>">
                      <?php echo ucfirst($row->class_status) ?>
                    </span>
                  </td>
                  <!-- <td><?php //echo $row->added_by ?></td> -->
                  <td>
                    <a href="javascript:;" data-class_id = "<?php echo $row->id; ?>" title ="Add section" class="btn btn-sm btn-info btn-circle sectionAdd">
                      <i class="fa fa-plus"></i>
                    </a>
                    <a href="javascript:;" title ="Edit class" class="btn btn-sm btn-primary btn-circle editClass" data-id = "<?php echo $row->id ?>">
                      <i class="fa fa-pen"></i>
                    </a>
                    <a href="process/classes.php?id=<?php echo $row->id ?>" title ="Delete class" class="btn btn-sm btn-danger btn-circle" onclick="return confirm('Are you sure you want to delete this class?')">
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
                <?php
                }
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- Add Class Modal-->
      <div class="modal fade" id="addClassModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Add Class</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
            </div>
            <form action="process/classes.php" method = "post" id = "class-form">
              <div class="modal-body">
                <div class="form-group row">
                  <div class="col-3">
                    <label class="form-control-label" for="class_name">Class Title:</label>
                  </div>
                  
                  <div class="col-9">
                    <input type="text" class="form-control form-control-sm" id="class_name" name="class_name" required placeholder="Enter class name">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-3">
                    <label class="form-control-label text-center" for="class_detail" placeholder = "Enter class name">Class Detail:</label>
                  </div>
                  
                  <div class="col-9">
                    <textarea class="form-control form-control-sm" name="class_detail" id="class_detail"  rows="4" onresize="none" placeholder=" Enter description about class"></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-3">
                    <label class="form-control-label text-center" for="class_status" placeholder = "Enter class name">Class Status:</label>
                  </div>
                  <div class="col-9">
                    <select name="class_status" id="class_status" class="form-control form-control-sm">
                      <option value="active">active</option>
                      <option value="inactive">inactive</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <input type="hidden" name = 'class_id' id = 'class_id' value = ''>
                <button class="btn btn-danger" type="reset"><i class = "fa fa-times"></i> Reset</button>
                <button type= "submit" class="btn btn-success"><i class = "fa fa-paper-plane"></i> Save Class</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- Add Section Modal-->
      <div class="modal fade" id="addSectionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Add Section</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
            </div>
            <form action="process/section.php" method = "post" id = "section-form">
              <div class="modal-body">
                <div class="form-group row">
                  <div class="col-3">
                    <label class="form-control-label" for="section_name">Section Title:</label>
                  </div>
                  <div class="col-9">
                    <input type="text" class="form-control form-control-sm" id="section_name" name="section_name" required placeholder="Enter section name">
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <input type="hidden" name = 'section_id' id = 'section_id' value = ''>
                <input type="hidden" name = 'section_class_id' id = 'section_class_id' value = ''>
                <button class="btn btn-danger" type="reset"><i class = "fa fa-times"></i> Reset</button>
                <button type= "submit" class="btn btn-success"><i class = "fa fa-paper-plane"></i> Save Section</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
    <?php require_once 'inc/copyRight.php'; ?>
  </div>
  <!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>
<?php require_once 'inc/footer.php'; ?>
<script>
$('.addModal').click(function(e){
$('#class_name').val(null);
$('#class_detail').val(null);
$('#class_status').val('active');
$('#class_id').val(null);
$('#addClassModal').modal('show');
});
$('.editClass').click(function (e){
e.preventDefault();
var editId = $(this).data('id');
$.ajax({
url: "./process/api.php",
type: "post",
data:{
class_id: editId,
act: "get.class"
},
success:function(response){
if(typeof(response) != "object"){
response = JSON.parse(response);
}
if(response.status){
// console.log(response.data[0]);
$('#class_name').val(response.data[0].class_name);
$('#class_detail').val(response.data[0].class_detail);
$('#class_status').val(response.data[0].class_status);
$('#class_id').val(response.data[0].id);
$('#addClassModal').modal('show');
}
}
});
});
$('.sectionAdd').click(function (e){
e.preventDefault();
$('#section_class_id').val($(this).data('class_id'));
$('#addSectionModal').modal('show');
});
// for section list
$('.show-section').click(function(event) {
$(this).parent().find('ul').toggleClass('collapse');
});
// for section edit
$('.edit_section').click(function(e) {
e.preventDefault();
var data = $(this).data('data');
if(typeof(data) != "object"){
data = JSON.parse(data);
}
$('#section_name').val(data.section_name);
$('#section_id').val(data.id);
$('#section_class_id').val($(this).data('class_id'));
$('#addSectionModal').modal('show');
});
</script>