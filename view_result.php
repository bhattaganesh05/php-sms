<?php
require_once "config/init.php";
require_once "inc/checkLogin.php";
$_title = "Result View || " . SITE_TITLE;
require_once "inc/header.php";
$exam = new Exam;
$result = new Result;
if (isset($_GET, $_GET['id']) && !empty($_GET['id'])) {
    $id = (int) $_GET['id'];
    if ($id <= 0) {
        redirect('../exams.php', 'error', 'Invalid exam id.');
    }
    $exam_info = $exam->getRowByRowId($id);
    // debug($exam_info,true);
    if (!$exam_info) {
        redirect('../exams.php', 'error', 'Exam does not exist.');
    }
    $result_info = $result->getResultByExamId($id);
    // debug($result_info,true);
}

?>
<div id="wrapper">
  <?php require_once 'inc/sidebar.php';?>
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
      <?php require_once 'inc/topbar.php';?>
      <!-- Begin Page Content -->
      <div class="container-fluid">
        <?php flash();?>
        <!-- Page Heading -->
        <h3 class="h4 mb-4 text-gray-800 font-weight-bold">Result for <?php echo $exam_info[0]->title ?> Exam
         </h3>
        <hr>
        <div class="row">
          <div class="col-12">
            <table class="table table-bordered table-hover table-sm text-center">
              <thead class="thead-dark">
                <th>Name</th>
                <th>Class</th>
                <th>Result</th>
                <th>Total</th>
                <th>Percentage</th>
                <th>View</th>
              </thead>
              <tbody>
<?php
            if($result_info){
                foreach ($result_info as  $row) {
                    $student = new Student;
                    $student_info = $student->getStudentByid($row->student_id);
                }
            }
 ?>             <tr class = "<?php echo $row->result == 'fail' ? 'table-danger' : 'table-success' ?>">
                <td><?php echo $student_info[0]->name ?></td>
                <td><?php echo $student_info[0]->class_name; if($student_info[0]->section_name) echo " (".$student_info[0]->section_name.")" ?></td>
                <td><?php echo $row->result ?></td>
                <td><?php echo $row->total_obtained_score ?></td>
                <td><?php echo $row->percentage ?></td>
                <td><a href="student_score.php?exam_id=<?php echo $row->exam_id ?>&student_id=<?php echo $row->student_id ?>"class="btn btn-sm btn-info btn-circle">
                        <i class="fa fa-eye"></i>
                    </a>
                </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
    <?php require_once 'inc/copyRight.php';?>
  </div>
  <!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>
<?php require_once 'inc/footer.php';?>
