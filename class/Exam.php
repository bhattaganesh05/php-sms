<?php
final class Exam extends Database
{
    use DataTrait;
    public function __construct()
    {
        parent::__construct();
        $this->table = "exams";
    }
    final public function getAllExamInfo(){
        $attr = array(
            'fields' => array(
                'exams.id',
                'exams.title',
                'classes.class_name',
                'class_section.section_name',
                'start_date',
                'end_date',
                'status'
            ),
            'leftJoin' => " LEFT JOIN classes ON classes.id = exams.class_id
                            LEFT JOIN class_section ON class_section.id = exams.section_id
                            ",
            'where' => "exams.end_date <= '".date("Y-m-d")."' "
        );
        return $this->select($attr);
    }
    final public function getCompletedExamByClass($class_id,$section_id){
$attr = array(
    'fields' => array(
        'exams.id',
        'exams.title'
    ),
    'where' => "end_date <= '" . date("Y-m-d") . "' AND class_id = ".$class_id." ",
);
if($section_id != NULL){
    $attr['where'] .= " AND section_id = ".$section_id;
}
return $this->select($attr);

    }

}
