<?php
final class Score extends Database
{
    use DataTrait;
    public function __construct()
    {
        parent::__construct();
        $this->table = "students_score";
    }
    public function getSchueduleAndScoreByExamId($exam_id,$student_id){
        $attr = array(
            'fields' => " students_score.*, subjects.subject_name  ",
            'leftJoin' => " LEFT JOIN subjects ON subjects.id = students_score.subject_id ",
            'where' => array(
                'exam_id' => $exam_id,
                'student_id' => $student_id
            )
        );
        return $this->select($attr);
    }
    public function getScoreId($exam_id,$student_id,$sub){
        $attr = array(
            'fields' => " id as score_id  ",
            'where' => array(
                'exam_id' => $exam_id,
                'student_id' => $student_id,
                'subject_id' => $sub
            )
        );
        return $this->select($attr);
    }
}
