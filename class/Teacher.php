<?php
final class Teacher extends Database
{
    use DataTrait;
    public function __construct()
    {
        parent::__construct();
        $this->table = "teachers";
    }

    public function getAllTeacherInfo(){
        $attr = array(
            'fields' => array(
                'teachers.id',
                'teachers.user_id',
                'teachers.education',
                'teachers.experience',
                'subjects.subject_name',
                'users.name as user_name',
                'users.status',
                'classes.class_name',
                'class_section.section_name'
            ),
            'leftJoin' => " LEFT JOIN users ON users.id = teachers.user_id
                            LEFT JOIN classes ON classes.id = teachers.class_id
                            LEFT JOIN subjects ON subjects.id = teachers.subject_id
                            LEFT JOIN class_section ON class_section.id = teachers.section_id ",
        );
        return $this->select($attr);
    }
}
