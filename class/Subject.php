<?php
final class Subject extends Database{
    use DataTrait;
    public function __construct(){
        parent::__construct();
        $this->table = "subjects";
    }
    public function getClassInfo(){
        $attr = array(
            'fields' => array(
                'subjects.id',
                'subjects.subject_name',
                'subjects.subject_code',
                'subjects.subject_desc',
                'subjects.status',
                'classes.class_name',
                'users.name'
            ),
            'leftJoin' => " LEFT JOIN classes ON classes.id = subjects.class_id
                            LEFT JOIN users ON users.id = subjects.teacher_id "
        );
        return $this->select($attr);
    }
    public function getSubjectByClass($class_id){
    	$attr = array(
    		'where' => array(
    			'class_id' => $class_id
    		)
    	);
    	return $this->select($attr);
    }
}