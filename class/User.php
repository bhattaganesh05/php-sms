<?php
final class User extends Database{
    use DataTrait;
    public function __construct(){
        parent::__construct();
        $this->table = "users";
    }
    public function getUserByEmail($email){
        $options = array(
            // 'fields' => ['id','name','email','password','role','status'],
            // 'fields'=> "id, email, password ",
            'where' => "email = '".$email."'"
            // 'where' => ['email'=>$email,"status"=>"active"]
        );
        return $this->select($options);
    }
    public function getUserByCookieToken($token){
        $options = array(
            'where' => ['remember_token' => $token]
        );
        return $this->select($options);
    }
    public function getUserByType($type){
        $options = array(
            'where' => ['role' => $type]
        );
        return $this->select($options);
    }
}