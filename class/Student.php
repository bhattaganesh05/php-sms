<?php
final class Student extends Database
{
    use DataTrait;
    public function __construct()
    {
        parent::__construct();
        $this->table = "students";
    }
    public function getAllStudnetInfo(){
        $attr = array(
            'fields' => array(
                'students.id',
                'students.user_id',
                'students.roll_no',
                'users.name as user_name',
                'users.status',
                'classes.class_name',
                'class_section.section_name'
            ),
            'leftJoin' => " LEFT JOIN users ON users.id = students.user_id
                            LEFT JOIN classes ON classes.id = students.class_id
                            LEFT JOIN class_section ON class_section.id = students.section_id ",
        );
        return $this->select($attr);
    }
    public function getRollno($class_id,$section_id = null){
        $attr = array(
            'fields' => "roll_no",
            'where' => "class_id = ".$class_id,
            'order_by' => "id DESC ",
            'limit' => " 0,1"
        );
        if($section_id){
            $attr['where'] .= " AND section_id = ".$section_id;
        }
        $roll_no = $this->select($attr);
        // debug($roll_no, true);

        if(!$roll_no){
            $roll_no = 1;
        }else{
            $roll_no = $roll_no[0]->roll_no;
            $roll_no += 1;
        }
        return $roll_no;
    }
    public function getStudentByid($student_id){
        $attr = array(
            'fields' => array(
                'students.*',
                'users.name',
                'users.image',
                'users.email',
                'classes.class_name',
                'class_section.section_name'
            ),
            'leftJoin' => " LEFT JOIN users ON users.id = students.user_id
                            LEFT JOIN classes ON classes.id = students.class_id
                            LEFT JOIN class_section ON class_section.id = students.section_id
                         ",
            'where' => " students.id = ".$student_id
        );
        return $this->select($attr);
    }
}
