<?php 
    abstract class Database{
        public $conn = "";
        protected $sql = "";
        protected $stmt = "";
        protected $table = null;
// DB connection
        public function __construct(){
            try{
                $this->conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME.";", DB_USER, DB_PWD);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->sql = "SET NAMES utf8";
                $this->stmt = $this->conn->prepare($this->sql);
                $this->stmt->execute();
            } catch(PDOException $e){
                $msg = date("Y-m-d h:i:s A")." Connection, PDO: ".$e->getMessage()."\r\n";
                error_log($msg, 3, ERROR_LOG);
            } catch(Exception $e){
                $msg = date("Y-m-d h:i:s A")." Connection, General: ".$e->getMessage()."\r\n";
                error_log($msg, 3, ERROR_LOG);
            }
        }
//select query
        protected final function select($attr = array(),$is_debug = false){
            try{
                $this->sql = "SELECT ";
                if(isset($attr['fields'])){
                    if(is_array($attr['fields'])){
                        $this->sql .= implode(", ",$attr['fields']);
                    }else {
                        $this->sql .= $attr['fields'];
                    }
                }else{
                    $this->sql .= " * ";
                }
                $this->sql .= " FROM ";
                if(!isset($this->table) && empty($this->table)){
                    throw new PDOExcetion(" Table  not set.");
                }
                $this->sql .= $this->table;
                if(isset($attr['leftJoin']) && !empty($attr['leftJoin'])){
                    $this->sql .=$attr['leftJoin'];
                }
                if(isset($attr['where'])){
                    if(is_string($attr['where'])){
                        $this->sql .=" WHERE ".$attr['where'];
                    }else{
                        $temp = array();
                        foreach($attr['where'] as $column_name => $value){
                            $str = $column_name.' = :'.$column_name;
                            $temp[] = $str;
                        }

                        $this->sql .= " WHERE ".implode(" AND ", $temp);
                    }
                }
                if(isset($attr['order_by']) && !empty($attr['order_by'])){
                    $this->sql .= " ORDER BY ".$attr['order_by'];
                }
                if(isset($attr['limit']) && !empty($attr['limit'])){
                    $this->sql .= "LIMIT".$attr['limit'];
                }

                if($is_debug){
                    debug($attr);
                    debug($this->sql);
                }
                    // debug($this->sql,true);

                $this->stmt = $this->conn->prepare($this->sql);
                if(isset($attr['where']) && !empty($attr['where']) && is_array($attr['where'])){
                    foreach($attr['where'] as $column_name => $value){
                        if(is_int($value)){
                            $param = PDO::PARAM_INT;
                        }elseif(is_bool($value)){
                            $param = PDO::PARAM_BOOL;
                        }else {
                            $param = PDO::PARAM_STR;
                        }
                        $this->stmt->bindValue(':'.$column_name,$value,$param);
                    }
                }

                $this->stmt->execute();
                $data = $this->stmt->fetchAll(PDO::FETCH_OBJ);
                return $data;
            } catch(PDOException $e){
                $msg = date("Y-m-d h:i:s A")." SELECT, PDO: ".$e->getMessage()."\r\n";
                error_log($msg, 3, ERROR_LOG);
            } catch(Exception $e){
                $msg = date("Y-m-d h:i:s A")." SELECT, General: ".$e->getMessage()."\r\n";
                error_log($msg, 3, ERROR_LOG);
            }
        }
// Update query
        protected final function update($data = array(),$attr = array(),$is_debug = false){
            try{
                $this->sql = "UPDATE ";
                if(!isset($this->table) && empty($this->table)){
                    throw new PDOExcetion(" Table  not set.");
                }
                $this->sql .= $this->table." SET ";
                if(isset($data) && !empty($data)){
                    if(is_string($data)){
                        $this->sql .= $data; 
                    }else{
                        $temp_data = array();
                        foreach ($data as $column_name => $value) {
                            $str_data = $column_name." = :_".$column_name;
                            $temp_data [] = $str_data;
                        }
                        $this->sql .= implode(", ",$temp_data);
                    }
                }else{
                    throw new Exception("Data not set");
                }
                if(isset($attr['where'])){
                    if(is_string($attr['where'])){
                        $this->sql .=" WHERE ".$attr['where'];
                    }else{
                        $temp = array();
                        foreach($attr['where'] as $column_name => $value){
                            $str = $column_name.' = :'.$column_name;
                            $temp[] = $str;
                        }

                        $this->sql .= " WHERE ".implode(" AND ", $temp);
                    }
                }  
                if($is_debug){
                    debug($data);
                    debug($attr);
                    debug($this->sql);
                }
                $this->stmt = $this->conn->prepare($this->sql);
                if(isset($data) && !empty($data) && is_array($data)){
                    foreach($data as $column_name => $value){
                        if(is_int($value)){
                            $param = PDO::PARAM_INT;
                        }elseif(is_bool($value)){
                            $param = PDO::PARAM_BOOL;
                        }else {
                            $param = PDO::PARAM_STR;
                        }
                        $this->stmt->bindValue(':_'.$column_name,$value,$param);
                    }
                }
                if(isset($attr['where']) && !empty($attr['where']) && is_array($attr['where'])){
                    foreach($attr['where'] as $column_name => $value){
                        if(is_int($value)){
                            $param = PDO::PARAM_INT;
                        }elseif(is_bool($value)){
                            $param = PDO::PARAM_BOOL;
                        }else {
                            $param = PDO::PARAM_STR;
                        }
                        $this->stmt->bindValue(':'.$column_name,$value,$param);
                    }
                }
                return $this->stmt->execute();
            } catch(PDOException $e){
                $msg = date("Y-m-d h:i:s A")." UPDATE, PDO: ".$e->getMessage()."\r\n";
                error_log($msg, 3, ERROR_LOG);
            } catch(Exception $e){
                $msg = date("Y-m-d h:i:s A")." UPDATE, General: ".$e->getMessage()."\r\n";
                error_log($msg, 3, ERROR_LOG);
            }
        }
// Insert query
        protected final function insert($data = array(),$is_debug = false){
            try{
                $this->sql = "INSERT INTO ";
                if(!isset($this->table) && empty($this->table)){
                    throw new PDOExcetion(" Table  not set.");
                }
                $this->sql .= $this->table." SET ";
                if(isset($data) && !empty($data)){
                    if(is_string($data)){
                        $this->sql .= $data; 
                    }else{
                        $temp_data = array();
                        foreach ($data as $column_name => $value) {
                            $str_data = $column_name." = :_".$column_name;
                            $temp_data [] = $str_data;
                        }
                        $this->sql .= implode(", ",$temp_data);
                    }
                }else{
                    throw new Exception("Data not set");
                }
                if($is_debug){
                    debug($data);
                    debug($this->sql);
                }
                $this->stmt = $this->conn->prepare($this->sql);
                if(isset($data) && !empty($data) && is_array($data)){
                    foreach($data as $column_name => $value){
                        if(is_int($value)){
                            $param = PDO::PARAM_INT;
                        }elseif(is_bool($value)){
                            $param = PDO::PARAM_BOOL;
                        }else {
                            $param = PDO::PARAM_STR;
                        }
                        $this->stmt->bindValue(':_'.$column_name,$value,$param);
                    }
                }
                $this->stmt->execute();
                return $this->conn->lastInsertId();
            } catch(PDOException $e){
                $msg = date("Y-m-d h:i:s A")." INSERT , PDO: ".$e->getMessage()."\r\n";
                error_log($msg, 3, ERROR_LOG);
            } catch(Exception $e){
                $msg = date("Y-m-d h:i:s A")." INSERT , General: ".$e->getMessage()."\r\n";
                error_log($msg, 3, ERROR_LOG);
            }
        }
// delete query
        protected final function delete($attr = array(),$is_debug = false){
            try{
                $this->sql = "DELETE FROM ";
                if(!isset($this->table) && empty($this->table)){
                    throw new PDOExcetion(" Table  not set.");
                }
                $this->sql .= $this->table;
                if(isset($attr['where'])){
                    if(is_string($attr['where'])){
                        $this->sql .=" WHERE ".$attr['where'];
                    }else{
                        $temp = array();
                        foreach($attr['where'] as $column_name => $value){
                            $str = $column_name.' = :'.$column_name;
                            $temp[] = $str;
                        }

                        $this->sql .= " WHERE ".implode(" AND ", $temp);
                    }
                }  
                if($is_debug){
                    debug($attr);
                    debug($this->sql);
                }
                $this->stmt = $this->conn->prepare($this->sql);
                if(isset($attr['where']) && !empty($attr['where']) && is_array($attr['where'])){
                    foreach($attr['where'] as $column_name => $value){
                        if(is_int($value)){
                            $param = PDO::PARAM_INT;
                        }elseif(is_bool($value)){
                            $param = PDO::PARAM_BOOL;
                        }else {
                            $param = PDO::PARAM_STR;
                        }
                        $this->stmt->bindValue(':'.$column_name,$value,$param);
                    }
                }
                return $this->stmt->execute();
            } catch(PDOException $e){
                $msg = date("Y-m-d h:i:s A")." DELETE, PDO: ".$e->getMessage()."\r\n";
                error_log($msg, 3, ERROR_LOG);
            } catch(Exception $e){
                $msg = date("Y-m-d h:i:s A")." DELETE, General: ".$e->getMessage()."\r\n";
                error_log($msg, 3, ERROR_LOG);
            }
        }
    }