<?php
final class Section extends Database{
    use DataTrait;
    public function __construct(){
        parent::__construct();
        $this->table = "class_section";
    }
    public function getSectionByClass($class_id){
    	$attr = array(
    		'where' => array(
    			'class_id' => $class_id
    		)
    	);
    	return $this->select($attr);
    }
}