<?php
final class Schedule extends Database
{
    use DataTrait;
    public function __construct()
    {
        parent::__construct();
        $this->table = "exams_schedule";
    }
    public function getSchueduleByExamId($exam_id){
        $attr = array(
            'fields' => " exams_schedule.* , subjects.subject_name ",
            'leftJoin' => " LEFT JOIN subjects ON subjects.id = exams_schedule.subject_id ",
            'where' => array(
                'exam_id' => $exam_id
            )
        );
        return $this->select($attr);
    }
    public function checkScheduleExists($exam_id,$subject_id){
        $attr = array(
            'where' => array(
                'exam_id' => $exam_id,
                'subject_id' => $subject_id
            )
        );
        return $this->select($attr);
    }
    public function getSubjectByExamId($exam_id,$subject_id){
        $attr = array(
            'where' => array(
                'exam_id' => $exam_id,
                'subject_id' => $subject_id,
            ),
        );
        return $this->select($attr);
    }
    public function getSchueduleAndScoreByExamId($exam_id,$student_id){
        $attr = array(
            'leftJoin' => " LEFT JOIN exams_schedule ON exams_schedule.id = students_score.subject_id
                            LEFT JOIN subjects ON subjects.id = students_score.subject_id 
                             ",
            'where' => array(
                'students_score.exam_id' => $exam_id,
                'students_score.student_id' => $student_id
            )
        );
        return $this->select($attr);
    }
}
