<?php
final class Result extends Database
{
    use DataTrait;
    public function __construct()
    {
        parent::__construct();
        $this->table = "students_result";
    }
    public function getResultIdByExamId($exam_id,$student_id){
        $attr = array(
            'fields' => " id as result_id ",
            'where' => array(
                'exam_id' => $exam_id,
                'student_id' => $student_id,
            ),
        );
        return $this->select($attr);
    }
    public function getResultByExamId($exam_id){
        $attr = array(
            'where' => array(
                'exam_id' => $exam_id,
            )
        );
        return $this->select($attr);
    }
}
