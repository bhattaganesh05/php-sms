<?php
require_once "config/init.php";
require_once "inc/checkLogin.php";
$_title = "Exams List || " . SITE_TITLE;
require_once "inc/header.php";
$exam = new Exam;
?>
<div id="wrapper">
  <?php require_once 'inc/sidebar.php';?>
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
      <?php require_once 'inc/topbar.php';?>
      <!-- Begin Page Content -->
      <div class="container-fluid">
        <?php flash();?>
        <!-- Page Heading -->
        <h3 class="h4 mb-4 text-gray-800 font-weight-bold">Exams List
         </h3>
        <hr>
        <div class="row">
          <div class="col-12">
            <table class="table table-bordered table-hover table-sm text-center">
              <thead class="thead-dark">
                <th>Title</th>
                <th>Class</th>
                <th>Section</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Status</th>
                <th>Action</th>
              </thead>
              <tbody>
                <?php

$all_data = $exam->getAllExamInfo();
// debug($all_data,true);
if ($all_data) {
    foreach ($all_data as $key => $row) {
        ?>
                <tr>
                  <td><?php echo $row->title ?></td>
                  <td><?php echo $row->class_name ?></td>
                  <td><?php echo (($row->section_name)) ? " Section " . $row->section_name : '' ?></td>
                  <td><?php echo $row->start_date ?></td>
                  <td><?php echo $row->end_date ?></td>
                  <td>
                    <span class = "badge badge-<?php echo ($row->status == 'upcoming') ? 'success' : 'danger'; ?>">
                      <?php echo ucfirst($row->status) ?>
                    </span>
                  </td>
                  <td>
                  <?php
$start_date = strtotime($row->start_date);
        $end_date = strtotime($row->end_date);
        $today = strtotime(date('Y-m-d'));
        if ($start_date > $today && $end_date > $today) {
            ?>
                    <a href="exam-form.php?id=<?php echo $row->id ?>" title ="Edit class" class="btn btn-sm btn-primary btn-circle editClass" data-id = "<?php echo $row->id ?>">
                      <i class="fa fa-pen"></i>
                    </a>
                    <a href="process/exam.php?id=<?php echo $row->id ?>" onclick="return confirm('Are you sure you want to delete this exam?')" title ="Delete exam" class="btn btn-sm btn-danger btn-circle">
                      <i class="fa fa-trash"></i>
                    </a>
                  <?php } else {?>
                            <a href="view_result.php?id=<?php echo $row->id ?>" class="btn btn-sm btn-info btn-circle viewResult">
                              <i class="fa fa-eye"></i>
                            </a>
                   <?php }

        ?>
                  </td>
                </tr>
                <?php
}
}
?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
    <?php require_once 'inc/copyRight.php';?>
  </div>
  <!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>
<?php require_once 'inc/footer.php';?>
