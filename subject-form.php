<?php
require_once "config/init.php";
require_once "inc/checkLogin.php";
$_title = "Subjects Form || ".SITE_TITLE;
$subject = new Subject;
$act =  "add";
if(isset($_GET, $_GET['id']) && !empty($_GET['id'])){
  $act =  "update";
  $id = (int)$_GET['id'];
  if($id<=0){
    redirect('../subject.php','error','Invalid subject id.');
  }
  $subject_info =  $subject->getRowByRowId($id);
  if(!$subject_info){
    redirect('../subject.php','error','Subject does not exist.');
  }
  // debug($subject_info,true);
}
require_once "inc/header.php";
?>
<div id="wrapper">
  <?php require_once 'inc/sidebar.php'; ?>
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
      <?php require_once 'inc/topbar.php'; ?>
      <!-- Begin Page Content -->
      <div class="container-fluid">
        <?php flash(); ?>
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800 font-weight-bold">Subjects Form</h1>
        <hr>
        <div class="row">
          <div class="col-12">
            <form action="process/subject.php" class="form" method="post">
              <div class="form-group row">
                <label for="subject_name" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Subject Title:</label>
                <div class="col-sm-12 col-md-9">
                  <input type="text" name="subject_name" id="subject_name" value ="<?php echo @$subject_info[0]->subject_name ?>"  class="form-control form-control-sm" required placeholder="Enter subject name">
                </div>
              </div>
              <?php if($act == "add"){ ?>
              <div class="form-group row">
                <label for="subject_code" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Subject Code:</label>
                <div class="col-sm-12 col-md-9">
                  <input type="text" name="subject_code" id="subject_code"  class="form-control form-control-sm" required  placeholder=" Enter subject code">
                </div>
              </div>
              <?php }?>
              <div class="form-group row">
                <label for="subject_Description" class="col-sm-12 col-md-3 form-control-label">Subject Description:</label>
                <div class="col-sm-12 col-md-9">
                  <textarea name="subject_Description" id="subject_Description"   rows="4" class="form-control form-control-sm" style="resize: none;" placeholder="Enter Description for Subject"><?php echo @$subject_info[0]->subject_desc ?></textarea>
                </div>
              </div>
              <div class="form-group row">
                <label for="subject_name" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Select Class:</label>
                <div class="col-sm-12 col-md-9">
                <select name="class_id" id="class_id" class="form-control form-control-sm" required>
                  <option value="" selected disabled>__Select-Class__</option>
                  <?php 
                      $class = new Classes;
                      $class_data = $class->selectAllRows();
                      foreach ($class_data as $key => $value) {
                        ?>
                      <option value="<?php echo $value->id ?>" <?php echo (isset($subject_info) && $subject_info[0]->class_id == $value->id) ? 'selected':''?>>
                        <?php echo $value->class_name ?></option>
                        <?php
                      }
                  ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="subject_name" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Subject Teacher:</label>
                <div class="col-sm-12 col-md-9">
                <select name="teacher_id" id="teacher_id" class="form-control form-control-sm" required>
                  <option value="" selected disabled>__Select-Teacher__</option>
                  <?php 
                      $user = new User;
                      $user_data = $user->getUserByType('teacher');
                      foreach ($user_data as $key => $value) {
                        ?>
                      <option value="<?php echo $value->id ?>" <?php echo (isset($subject_info) && $subject_info[0]->teacher_id == $value->id) ? 'selected':''?>>
                        <?php echo $value->name ?></option>
                        <?php
                      }
                  ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="subject_name" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Subject Status:</label>
                <div class="col-sm-12 col-md-9">
                <select name="status" id="status" class="form-control form-control-sm" required>
                  <option value="active" <?php echo (isset($subject_info) && $subject_info[0]->status == 'active') ? 'selected':''?>>active</option>
                  <option value="inactive" <?php echo (isset($subject_info) && $subject_info[0]->status == 'inactive') ? 'selected':''?>>inactive</option>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-12 col-md-9 offset-md-3">
                  <button class="btn btn-danger btn-sm" type="reset"><i class = "fa fa-times"></i> Reset</button>
                  <button type= "submit" class="btn btn-success btn-sm"><i class = "fa fa-paper-plane"></i> Add Subject</button>
                </div>
              </div>
              <input type="hidden" name="subject_id" value="<?php echo @$subject_info[0]->id ?>">
            </form>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
    <?php require_once 'inc/copyRight.php'; ?>
  </div>
  <!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>
<?php require_once 'inc/footer.php'; ?>
