<?php
require_once "config/init.php";
require_once "inc/checkLogin.php";
$_title = "Student Score Form || " . SITE_TITLE;
$student = new Student;
$score = new Score;
$exam = new Exam;
if (isset($_GET, $_GET['student_id']) && !empty($_GET['student_id'])) {
    $act = "update";
    $student_id = (int) $_GET['student_id'];
    if ($student_id <= 0) {
        redirect('../students.php', 'error', 'Invalid student id.');
    }
    $student_info = $student->getStudentByid($student_id);
    // debug($student_info,true);
    if (!$student_info) {
        redirect('../students.php', 'error', 'Student does not exist.');
    }
    // $exam_id = 8;
    // $student_id = 19;
    if(isset($_GET['exam_id']) && !empty($_GET['exam_id'])){
        $exam_id = (int) $_GET['exam_id'];
    }
    $exam_info = $exam->getRowByRowId($exam_id);
    $scores = $score->getSchueduleAndScoreByExamId($exam_id,$student_id);
    // debug($scores, true);

}
require_once "inc/header.php";
?>
<div id="wrapper">
  <?php require_once 'inc/sidebar.php';?>
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
      <?php require_once 'inc/topbar.php';?>
      <!-- Begin Page Content -->
        <div class="container-fluid">
            <?php flash();?>
            <!-- Page Heading -->
            <h3 class="h3 mb-4 text-gray-800 font-weight-bold">Student MarkSheet for <?php echo "<em>".$exam_info[0]->title."</em>" ?></h3>
            <hr>
            <div class="row">
                <div class="col-12">
                    <h5 class = 'text-info text-center'>Student Information</h5>
                    <div class="row">
                        <div class="col-3"><strong>Name:</strong></div>
                        <div class="col-9"><span><?php echo @$student_info[0]->name ?></span></div>
                    </div>
                    <div class="row">
                        <div class="col-3"><strong>Email:</strong></div>
                        <div class="col-9"><span><?php echo @$student_info[0]->email ?></span></div>
                    </div>
                    <div class="row">
                        <div class="col-3"><strong>Class:</strong></div>
                        <div class="col-9"><span><?php echo @$student_info[0]->class_name ?></span></div>
                    </div>
                    <?php if (isset($student_info[0]->section_name)): ?>
                    <div class="row">
                        <div class="col-3"><strong>Section:</strong></div>
                        <div class="col-9"><span><?php echo @$student_info[0]->section_name ?></span></div>
                    </div>
                    <?php endif;?>
                    <div class="row">
                        <div class="col-3"><strong>Roll No.:</strong></div>
                        <div class="col-9"><span><?php echo @$student_info[0]->roll_no ?></span></div>
                    </div>
                </div>
            </div> 
            <hr>
            <div class="row">
                <div class="col-3">
                    <strong class = 'text-center'>Subject</strong>
                </div>
                <div class="col-3">
                    <strong class = 'text-center'>Full Mark</strong>
                </div>
                <div class="col-3">
                    <strong class = 'text-center'>Pass Mark</strong>
                </div>
                <div class="col-3">
                    <strong class = 'text-center'>Obtained Mark</strong>
                </div>
            </div>
            <hr>
            <div id = "subs">
            <?php 
                foreach ($scores as $key => $scores) {
                    $schedule = new Schedule;
                    $schedules = $schedule->getSubjectByExamId($scores->exam_id,$scores->subject_id);
            ?>
                <div class="row">
                    <div class="col-3">
                        <strong class = 'text-center'><?php echo $scores->subject_name  ?></strong>
                    </div>
                    <div class="col-3">
                        <strong class = 'text-center'><?php echo $schedules[0]->full_marks  ?></strong>
                    </div>
                    <div class="col-3">
                        <strong class = 'text-center'><?php echo $schedules[0]->pass_marks ?>
</strong>
                    </div>
                    <div class="col-3">
                        <strong class = 'text-center'><?php echo $scores->obtained_score ?>
</strong>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
      <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
    <?php require_once 'inc/copyRight.php';?>
  </div>
  <!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>
<?php require_once 'inc/footer.php';?>