<?php
require_once "config/init.php";
require_once "inc/checkLogin.php";
$_title = "Teachers List || " . SITE_TITLE;
require_once "inc/header.php";
$teacher = new Teacher;
?>
<div id="wrapper">
  <?php require_once 'inc/sidebar.php';?>
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
      <?php require_once 'inc/topbar.php';?>
      <!-- Begin Page Content -->
      <div class="container-fluid">
        <?php flash();?>
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800 font-weight-bold">Teachers List
        <a href="teacher-form.php"  class=" addModal btn btn-success btn-sm float-right"> <i class="fa fa-plus"></i> Add Teacher</a>
        </h1>
        <hr>
        <div class="row">
          <div class="col-12">
            <table class="table table-bordered table-hover table-sm text-center">
              <thead class="thead-dark">
                <th>Name</th>
                <th>Class</th>
                <th>Section</th>
                <th>Subject</th>
                <th>Education</th>
                <th>Experience</th>
                <th>Status</th>
                <th>Action</th>
              </thead>
              <tbody>
                <?php

$all_data = $teacher->getAllTeacherInfo();
// debug($all_data,true);
if ($all_data) {
    foreach ($all_data as $key => $row) {
        ?>
                <tr>
                  <td><?php echo $row->user_name ?></td>
                  <td><?php echo $row->class_name ?></td>
                  <td><?php echo $row->section_name ?></td>
                  <td><?php echo $row->subject_name ?></td>
                  <td><?php echo $row->education ?></td>
                  <td><?php echo $row->experience  ?></td>
                  <td>
                    <span class = "badge badge-<?php echo ($row->status == 'active') ? 'success' : 'danger'; ?>">
                      <?php echo ucfirst($row->status) ?>
                    </span>
                  </td>
                  <td>
                    <a href="teacher-form.php?id=<?php echo $row->id ?>" title ="Edit class" class="btn btn-sm btn-primary btn-circle editClass" data-id = "<?php echo $row->id ?>">
                      <i class="fa fa-pen"></i>
                    </a>
                    <a href="process/teacher.php?id=<?php echo $row->id ?>" onclick="return confirm('Are you sure you want to delete this teacher?')" title ="Delete class" class="btn btn-sm btn-danger btn-circle" onclick="return confirm('Are you sure you want to delete this teacher?')">
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
                <?php
}
}
?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
    <?php require_once 'inc/copyRight.php';?>
  </div>
  <!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>
<?php require_once 'inc/footer.php';?>
