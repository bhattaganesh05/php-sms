<?php
    ob_start();
    session_start();
    date_default_timezone_set("Asia/kathmandu");

    define("SITE_URL", "http://sms.loc");

    define("DB_HOST", "localhost");
    define("DB_NAME", "php_sms");
    define("DB_USER", "root");
    define("DB_PWD", "");

    define("ERROR_LOG", $_SERVER['DOCUMENT_ROOT']."/error/error.log");
    define("SITE_TITLE", "School Management System");

    define("ASSETS_URL", SITE_URL."/assets");
    define("CSS_URL", ASSETS_URL."/css");
    define("JS_URL", ASSETS_URL."/js");
    define("UPLOAD_DIR",$_SERVER['DOCUMENT_ROOT']."/uploads");
    define("UPLOAD_URL",SITE_URL."/uploads");
    define("IMAGES_URL", ASSETS_URL."/img");
    define("ALLOWED_IMAGES",array('jpg','png','jpeg','gif','bmp'));
    define("PLUGINS_URL", ASSETS_URL."/plugins");


    