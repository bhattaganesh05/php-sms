<?php
require_once "config/init.php";
require_once "inc/checkLogin.php";
$_title = "Teachers Form || " . SITE_TITLE;
$teacher = new Teacher;
$user = new User;
$act = "add";
if (isset($_GET, $_GET['id']) && !empty($_GET['id'])) {
    $act = "update";
    $id = (int) $_GET['id'];
    if ($id <= 0) {
        redirect('../teachers.php', 'error', 'Invalid teacher id.');
    }
    $teacher_info = $teacher->getRowByRowId($id);
    if (!$teacher_info) {
        redirect('../teachers.php', 'error', 'Teacher does not exist.');
    }
    $user_info = $user->getRowByRowId($teacher_info[0]->user_id);
    // debug($teacher_info,true);
}
require_once "inc/header.php";
?>
<div id="wrapper">
  <?php require_once 'inc/sidebar.php';?>
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
      <?php require_once 'inc/topbar.php';?>
      <!-- Begin Page Content -->
      <div class="container-fluid">
        <?php flash();?>
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800 font-weight-bold">Teachers Form</h1>
        <hr>
        <div class="row">
          <div class="col-12">
            <form action="process/teacher.php" class="form" method="post" enctype = "multipart/form-data">
              <div class="form-group row">
                <label for="teacher_name" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Teacher Name:</label>
                <div class="col-sm-12 col-md-9">
                  <input type="text" name="teacher_name" id="teacher_name" value ="<?php echo @$user_info[0]->name ?>"  class="form-control form-control-sm" required placeholder="Enter  name">
                </div>
              </div>
              <?php if ($act == "add"): ?>
              <div class="form-group row">
                <label for="teacher_email" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Teacher Email:</label>
                <div class="col-sm-12 col-md-9">
                  <input type="email" name="teacher_email" id="teacher_email"   class="form-control form-control-sm" required placeholder="Enter email">
                </div>
              </div>
              <div class="form-group row">
                <label for="teacher_password" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Password:</label>
                <div class="col-sm-12 col-md-9">
                  <input type="password" name="teacher_password" id="teacher_password"  class="form-control form-control-sm" required placeholder="Enter password">
                </div>
              </div>
              <div class="form-group row">
                <label for="teacher_repassword" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Confirm-password:</label>
                <div class="col-sm-12 col-md-9">
                  <input type="password" name="teacher_repassword" id="teacher_repassword"  class="form-control form-control-sm" required placeholder="Re-enter password">
                </div>
              </div>
              <?php endif;?>
              <div class="form-group row">
                <label for="teacher_class_id" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Select Class:</label>
                <div class="col-sm-12 col-md-9">
                <select name="teacher_class_id" id="teacher_class_id" class="form-control form-control-sm" required>
                  <option value="" selected disabled>__Select-Class__</option>
                  <?php
$class = new Classes;
$class_data = $class->selectAllRows();
foreach ($class_data as $key => $value) {
    ?>
                      <option value="<?php echo $value->id ?>" <?php echo (isset($teacher_info) && $teacher_info[0]->class_id == $value->id) ? 'selected' : '' ?>>
                        <?php echo $value->class_name ?></option>
                        <?php
}
?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-sm-12 col-md-3 form-control-label">Select Section:</label>
                <div class="col-sm-12 col-md-9">
                <select name="teacher_section_id" id="teacher_section_id" class="form-control form-control-sm">
                <!--<option value="" disabled selected></option -->
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-sm-12 col-md-3 form-control-label">Select Subject:</label>
                <div class="col-sm-12 col-md-9">
                <select name="teacher_subject_id" id="teacher_subject_id" class="form-control form-control-sm">
                <!--<option value="" disabled selected></option -->
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="teacher_education" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Education:</label>
                <div class="col-sm-12 col-md-9">
                  <!-- <input type="text" name="teacher_education" id="teacher_education" value ="<?php echo @$user_info[0]->education ?>"  class="form-control form-control-sm" required placeholder="Enter  enter education"> -->
                  <select name="teacher_education" id="teacher_education" class="form-control form-control-sm">
                <?php
                  if($act == 'add'){
                    echo "<option value = '' disabled selected>__Select-Qualification__</option>";
                  }
                  $education_form = array('+2','Bachelors','Master Degree','M.Phil','P.H.D');
                  foreach ($education_form as $key => $value){ ?>
                    <option value="<?php echo $value ?>" <?php echo (isset($teacher_info) && $teacher_info[0]->education == $value) ? 'selected' : '' ?>><?php echo $value ?></option>
                  <?php } ?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="teacher_experience" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Experience in years:</label>
                <div class="col-sm-12 col-md-9">
                  <input type="number" name="teacher_experience" id="teacher_experience" value ="<?php echo @$teacher_info[0]->experience ?>" min = "0"  class="form-control form-control-sm" required placeholder="Enter  enter experience">
                </div>
              </div>
              <div class="form-group row">
                <label for="status" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Teacher Status:</label>
                <div class="col-sm-12 col-md-9">
                <select name="status" id="status" class="form-control form-control-sm" required>
                  <option value="active" <?php echo (isset($user_info) && $user_info[0]->status == 'active') ? 'selected' : '' ?>>active</option>
                  <option value="inactive" <?php echo (isset($user_info) && $user_info[0]->status == 'inactive') ? 'selected' : '' ?>>inactive</option>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="teacher_image" class="col-sm-4 col-md-3 form-control-label">Teacher Image:</label>
                <div class="col-sm-3 col-md-4">
                  <input type="file" name="teacher_image" id="teacher_image"  class=" form-control-file form-control-sm" accept = "image/*">
                </div>
                <div class="col-sm-5 col-5">
                      <?php if (isset($user_info) && !empty($user_info[0]->image) && file_exists(UPLOAD_DIR . "/teacher/" . $user_info[0]->image)) {?>
                        <img src="<?php echo UPLOAD_URL . '/teacher/' . $user_info[0]->image ?>" alt="" class=" img-profile" width = "100" height = "100">
                      <?php }?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-12 col-md-9 offset-md-3">
                  <button class="btn btn-danger btn-sm" type="reset"><i class = "fa fa-times"></i> Reset</button>
                  <button type= "submit" class="btn btn-success btn-sm"><i class = "fa fa-paper-plane"></i> Add Teacher</button>
                </div>
              </div>
              <input type="hidden" name="teacher_id" value="<?php echo @$teacher_info[0]->id ?>">
              <input type="hidden" name="user_id" value="<?php echo @$teacher_info[0]->user_id ?>">

            </form>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
    <?php require_once 'inc/copyRight.php';?>
  </div>
  <!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>
<?php require_once 'inc/footer.php';?>
<script>
    $('#teacher_class_id').change(function(e){
        var class_id = $(this).val();
        var section_id = "<?php echo (isset($teacher_info) && !empty($teacher_info[0]->section_id)) ? $teacher_info[0]->section_id : null ?>";
        var subject_id = "<?php echo (isset($teacher_info) && !empty($teacher_info[0]->subject_id)) ? $teacher_info[0]->subject_id : null ?>";
        // console.log(section_id);
        $.ajax({
            url: "process/api.php",
            method: "post",
            data: {
                class_id: class_id,
                act: "get-sections-with-subject"
            },
            success: function(response){
                if(typeof(response) != "object"){
                    response = JSON.parse(response);
                }
                // var opt_html = "<option value ='' disabled selected>__Select-Section__</option>";
                if(response.status){
                  // console.log(response);
                  if(response.data.section_data.length != 0){
                    var opt_html = "<option value ='' >__Select-Section__</option>";
                      $.each(response.data.section_data,function(key,value){
                          opt_html += "<option value = '"+value.id+"'";
                          if(value.id == section_id){
                            opt_html += " selected ";
                          }
                          opt_html += ">Section "+value.section_name+"</option>";
                          // console.log("Section "+value.section_name);
                      });
                  }else{
                    var opt_html = "<option value ='' disabled selected>No section</option>";
                  }
                  if(response.data.all_subjects.length != 0){
                    var sub_html = "<option value ='' >__Select-Subject__</option>";
                      $.each(response.data.all_subjects,function(key,value){
                          sub_html += "<option value = '"+value.id+"'";
                          if(value.id == subject_id){
                            sub_html += " selected ";
                          }
                          sub_html += ">"+value.subject_name+"</option>";
                          // console.log("Section "+value.section_name);
                      });
                  }else{
                    var sub_html = "<option value ='' disabled selected>No Subject</option>";
                  }
                }
                $('#teacher_section_id').html(opt_html);
                $('#teacher_subject_id').html(sub_html);
            }
        });
    });
    <?PHP
if ($act == "update") {
    ?>
       $('#teacher_class_id').change();
     <?php }?>
</script>
