<?php
require_once "config/init.php";
require_once "inc/checkLogin.php";
$_title = "Student Score Form || " . SITE_TITLE;
$exam = new Exam;
$student =  new Student;
$score =  new Score;
$act = "add";

if (isset($_GET, $_GET['id']) && !empty($_GET['id'])) {
    $act = "update";
    $student_id = (int) $_GET['id'];
    if ($student_id <= 0) {
        redirect('../students.php', 'error', 'Invalid student id.');
    }
    $student_info = $student->getStudentByid($student_id);
    // debug($student_info,true);
    if (!$student_info) {
        redirect('../students.php', 'error', 'Student does not exist.');
    }
    $exams = $exam->getCompletedExamByClass($student_info[0]->class_id,@$student_info[0]->$section_id);
    // debug($exams,true);
    // $exam_id = 8;
    // $student_id = 19;
// $scores = $score->getSchueduleAndScoreByExamId($exam_id,$student_id);
// debug($scores, true);
}
require_once "inc/header.php";
?>
<div id="wrapper">
  <?php require_once 'inc/sidebar.php';?>
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
      <?php require_once 'inc/topbar.php';?>
      <!-- Begin Page Content -->
      <div class="container-fluid">
        <?php flash();?>
        <!-- Page Heading -->
        <h3 class="h3 mb-4 text-gray-800 font-weight-bold">Student Score Form</h3>
        <hr>
        <div class="row">
          <div class="col-12">
            <form action="process/score.php" class="form" method="post" enctype = "multipart/form-data">
              <div class="row">
                <div class="col-6">
                    <img src="<?php echo UPLOAD_URL.'/student/'.$student_info[0]->image ?>" alt="" class=" img-thumbnail" height = "" width = "170">
                </div>
                <div class="col-6">
                  <h5 class = 'text-info text-center'>Student Information</h5>
                  <hr>
                  <div class="row">
                      <div class="col-3"><strong>Name:</strong></div>
                      <div class="col-9"><span><?php echo @$student_info[0]->name ?></span></div>
                  </div>
                  <div class="row">
                      <div class="col-3"><strong>Email:</strong></div>
                      <div class="col-9"><span><?php echo @$student_info[0]->email ?></span></div>
                  </div>
                  <div class="row">
                      <div class="col-3"><strong>Class:</strong></div>
                      <div class="col-9"><span><?php echo @$student_info[0]->class_name ?></span></div>
                  </div>
                  <?php if(isset($student_info[0]->section_name)): ?>
                  <div class="row">
                      <div class="col-3"><strong>Section:</strong></div>
                      <div class="col-9"><span><?php echo @$student_info[0]->section_name ?></span></div>
                  </div>
                  <?php endif; ?>
                  <div class="row">
                      <div class="col-3"><strong>Roll No.:</strong></div>
                      <div class="col-9"><span><?php echo @$student_info[0]->roll_no ?></span></div>
                  </div>
                  <hr>
                  <div class="row">
                      <div class="col-3"><strong>Select Exam:</strong></div>
                      <div class="col-9">
                        <select name="exam_id" id="exam_id" required class="form-control form-control-sm">
                        <option value="" disabled selected >__Select-Exam__</option>
                        <?php 
                        if($exams){
                          foreach ($exams as $key => $exam_data) {
                        ?>
                        <option value="<?php echo $exam_data->id ?>"><?php echo $exam_data->title ?></option>
                        
                  <?php }
                        }
                        ?>
                        </select>
                      </div>
                      
                  </div>
                </div>
              </div>
              <hr>
            <div class="row">
              <div class="col-3">
                <strong class = 'text-center'>Subject</strong>
              </div>
              <div class="col-3">
                <strong class = 'text-center'>Full Mark</strong>
              </div>
              <div class="col-3">
                <strong class = 'text-center'>Pass Mark</strong>
              </div>
              <div class="col-3">
                <strong class = 'text-center'>Obtained Mark</strong>
              </div>
              <input type="hidden" name="student_id" value="<?php echo @$student_info[0]->id ?>">
            </div>
            <hr>
                  <div class="row">
                    <div id="subs" class = 'col-12'></div>
                  </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
    <?php require_once 'inc/copyRight.php';?>
  </div>
  <!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>
<?php require_once 'inc/footer.php';?>
<script>
  $('#exam_id').change(function (){
    var exam_id = $(this).val();
    var student_id = "<?php echo $student_id ?>";
    getExamsSubs(exam_id,student_id);
  });
  function getExamsSubs(exam_id,student_id) {
    $.ajax({
      url : "process/api.php",
      type : "post",
      data : {
        // act : "get_exam-schedule",
        act : "get_exam_scedule_and_score",
        exam_id : exam_id,
        student_id : student_id
      },
      success: function(response){
        if(typeof(response) != "object"){
          response = JSON.parse(response);
        }
        console.log(response);
        var html_div = "";
        var button_html = "";
        if(response.status){
          if(response.data[0]){
            var counter = 0;
            $.each(response.data[0],function(key,value){
              html_div += "<div class='form-group row'>";
              html_div += "<label class = 'form-label col-3'>"+value.subject_name+"</label>";
              html_div += "<label class = 'form-label col-3'>"+value.full_marks+"</label>";
              html_div += "<label class = 'form-label col-3'>"+value.pass_marks+"</label>";
              html_div += "<div class = 'col-3'>";
              html_div += "<input type ='number' id = 'obtain_score"+key+"' name = 'score["+value.subject_id+"]' min = '0' required max = '"+value.full_marks+"' class ='form-control form-control-sm' >"
            //  html_div += "<input type = 'hidden' name = 'score["+value.id+"]' value = '"+value.id+"'>";
              html_div += "</div>";
              html_div += "</div>";
            });
          } 
          if(response.data[2]){
              html_div += "<input type = 'hidden' name = result_id[] value = '"+response.data[2][0].result_id+"'>";
          }
          html_div += "<hr>";
          html_div += "<button type = 'submit' class = 'btn btn-sm btn-success btn-block'><i class = 'fa fa-paper-plane'></i> Add Score</button>"
        }
        $('#subs').html(html_div);
        if(response.data[1]){
          $.each(response.data[1],function(key,value){
            $("#obtain_score"+key).attr("value",value.obtained_score);
          });
        }
      }
    });
  }
</script>
