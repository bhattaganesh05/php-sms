<?php
require_once "config/init.php";
require_once "inc/checkLogin.php";
$_title = "Exams Form || " . SITE_TITLE;
$exam = new Exam;
$schedule = new Schedule;
$act = "add";
if (isset($_GET, $_GET['id']) && !empty($_GET['id'])) {
    $act = "update";
    $id = (int) $_GET['id'];
    if ($id <= 0) {
        redirect('../exams.php', 'error', 'Invalid exam id.');
    }
    $exam_info = $exam->getRowByRowId($id);
    // debug($exam_info,true);
    if (!$exam_info) {
        redirect('../exams.php', 'error', 'Exam does not exist.');
    }
    $schedule_info = $schedule->getSchueduleByExamId($id);
    // debug($schedule_info,true);
}
require_once "inc/header.php";
?>
<div id="wrapper">
  <?php require_once 'inc/sidebar.php';?>
  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
      <?php require_once 'inc/topbar.php';?>
      <!-- Begin Page Content -->
      <div class="container-fluid">
        <?php flash();?>
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800 font-weight-bold">Exams Form</h1>
        <hr>
        <div class="row">
          <div class="col-12">
            <form action="process/exam.php" class="form" method="post" enctype = "multipart/form-data">
              <div class="form-group row">
                <label for="exam_title" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Exam Title:</label>
                <div class="col-sm-12 col-md-9">
                  <input type="text" name="exam_title" id="exam_title" value ="<?php echo @$exam_info[0]->title ?>"  class="form-control form-control-sm" required placeholder="Enter  title">
                </div>
              </div>
              <?php if(!isset($exam_info) && empty($exam_info)): ?>
              <div class="form-group row">
                <label for="exam_class_id" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Select Class:</label>
                <div class="col-sm-12 col-md-9">
                <select name="exam_class_id" id="exam_class_id" class="form-control form-control-sm" required>
                  <option value="" selected disabled>__Select-Class__</option>
                  <?php
$class = new Classes;
$class_data = $class->selectAllRows();
foreach ($class_data as $key => $value) {
    ?>
                      <option value="<?php echo $value->id ?>" <?php echo (isset($exam_info) && $exam_info[0]->class_id == $value->id) ? 'selected' : '' ?>>
                        <?php echo $value->class_name ?></option>
                        <?php
}
?>
                </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="col-sm-12 col-md-3 form-control-label">Select Section:</label>
                <div class="col-sm-12 col-md-9">
                <select name="exam_section_id" id="exam_section_id" class="form-control form-control-sm">
                <!--<option value="" disabled selected></option -->
                </select>
                </div>
              </div>
<?php endif; ?>
              <div class="form-group row">
                <label for="exam_start_date" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Exam Start Date:</label>
                <div class="col-sm-12 col-md-9">
                  <input type="date" name="exam_start_date" id="exam_start_date" value ="<?php echo @$exam_info[0]->start_date ?>"  class="form-control form-control-sm" required placeholder="Enter exam start date">
                </div>
              </div>
              <div class="form-group row">
                <label for="exam_end_date" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Exam Finish Date:</label>
                <div class="col-sm-12 col-md-9">
                  <input type="date" name="exam_end_date" id="exam_end_date" value ="<?php echo @$exam_info[0]->end_date ?>"  class="form-control form-control-sm" required placeholder="Enter finish date">
                </div>
              </div>
              <div class="form-group row">
                <label for="status" class="col-sm-12 col-md-3 form-control-label"><sup>*</sup>Exam Status:</label>
                <div class="col-sm-12 col-md-9">
                <select name="status" id="status" class="form-control form-control-sm" required>
                  <option value="upcoming" <?php echo (isset($exam_info) && $exam_info[0]->status == 'upcoming') ? 'selected' : '' ?>>upcoming</option>
                  <option value="ended" <?php echo (isset($exam_info) && $exam_info[0]->status == 'ended') ? 'selected' : '' ?>>ended</option>
                </select>
                </div>
              </div>
              <hr>
              <div class="row">
              <div class="col-12">
              <h4 class = 'text-left'>Subject Schedule</h4>
              </div>
              <div class="col-12">
                <div class="row">
                <div class="col-3"><h6 class = 'text-left'>Subject Name</h6></div>
                <div class="col-3"><h6 class = 'text-left'>Exam Date</h6></div>
                <div class="col-3"><h6 class = 'text-left'>Full Marks</h6></div>
                <div class="col-3"><h6 class = 'text-left'>Pass Marks</h6></div>
                </div>
              </div>
              </div>
              <hr>
              <div id="exam_schedule">
              <?php if(isset($schedule_info)){
                foreach ($schedule_info as $key => $value) {
                  ?>
                        <div class='form-group row'>
                         <label class='col-3'><?php echo $value->subject_name ?></label>
                         <div class = 'col-3'>
                         <input type='date' class = 'form-control form-control-sm' name = 'exam_schedule[<?php echo $value->subject_id ?>][date]' placeholder = 'Enter exam date' value = "<?php echo $value->exam_date ?>"  required />
                         </div>
                         <div class = 'col-3'>
                         <input type='number' class = 'form-control form-control-sm' min = '1' name = 'exam_schedule[<?php echo $value->subject_id ?>][full_marks]' placeholder = 'Enter full marks' value = "<?php echo $value->full_marks ?>"  required />
                         </div>
                         <div class = 'col-3'>
                         <input type='number' class = 'form-control form-control-sm' name = 'exam_schedule[<?php echo $value->subject_id ?>][pass_marks]'  placeholder = 'Enter pass marks' min = '1' value = "<?php echo $value->pass_marks ?>" required/>
                         </div>
                         </div>

                <?php } 
              }else{ ?>
              <div class="row">
                  <div class="col-12">
                  <p class="alert-info tex">Select Class First</p>
                  </div>    
              </div>
              <?php } ?>
              </div>
              <div class="form-group row">
                <div class="col-sm-12 col-md-9 offset-md-3">
                  <button class="btn btn-danger btn-sm" type="reset"><i class = "fa fa-times"></i> Reset</button>
                  <button type= "submit" class="btn btn-success btn-sm"><i class = "fa fa-paper-plane"></i> Add Exam</button>
                </div>
              </div>
              <input type="hidden" name="exam_id" value="<?php echo @$exam_info[0]->id ?>">
              <input type="hidden" name="user_id" value="<?php echo @$exam_info[0]->user_id ?>">

            </form>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
    <?php require_once 'inc/copyRight.php';?>
  </div>
  <!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>
<?php require_once 'inc/footer.php';?>
<script>
    $('#exam_class_id').change(function(e){
        var class_id = $(this).val();
        var section_id = "<?php echo (isset($exam_info) && !empty($exam_info[0]->section_id)) ? $exam_info[0]->section_id : null ?>";
        // var subject_id = "<?php //echo (isset($exam_info) && !empty($exam_info[0]->subject_id)) ? $exam_info[0]->subject_id : null ?>";
        // console.log(section_id);
        $.ajax({
            url: "process/api.php",
            method: "post",
            data: {
                class_id: class_id,
                act: "get-sections-with-subject"
            },
            success: function(response){
                if(typeof(response) != "object"){
                    response = JSON.parse(response);
                }
                if(response.status){
                  // console.log(response);
                  if(response.data.section_data.length != 0){
                    var opt_html = "<option value ='' >__Select-Section__</option>";
                      $.each(response.data.section_data,function(key,value){
                          opt_html += "<option value = '"+value.id+"'";
                          if(value.id == section_id){
                            opt_html += " selected ";
                          }
                          opt_html += ">Section "+value.section_name+"</option>";
                          // console.log("Section "+value.section_name);
                      });
                  }else{
                    var opt_html = "<option value ='' disabled selected>No section</option>";
                  }
                  <?php
                  if(!isset($schedule_info) || empty($schedule_info)){
                  ?>
                    if(response.data.all_subjects.length != 0){
                      var sub_html = "";
                      $.each(response.data.all_subjects,function(key,value){
                        sub_html += "<div class='form-group row'>";
                        sub_html += "<label class='col-3'>"+value.subject_name+"</label>";
                        sub_html += "<div class = 'col-3'>";
                        sub_html += "<input type='date' class = 'form-control form-control-sm' name = 'exam_schedule["+value.id+"][date]' placeholder = 'Enter exam date'  required />";
                        sub_html += "</div>";
                        sub_html += "<div class = 'col-3'>";
                        sub_html += "<input type='number' class = 'form-control form-control-sm' min = '1' name = 'exam_schedule["+value.id+"][full_marks]' placeholder = 'Enter full marks'  required />";
                        sub_html += "</div>"
                        sub_html += "<div class = 'col-3'>";
                        sub_html += "<input type='number' class = 'form-control form-control-sm' name = 'exam_schedule["+value.id+"][pass_marks]'  placeholder = 'Enter pass marks' min = '1' required/>";
                        sub_html += "</div>"
                        sub_html += "</div>";


                      });
                    }else{
                        var sub_html = "<option value ='' disabled selected>No Subject</option>";
                    }
                  <?php } ?>
                }
                $('#exam_section_id').html(opt_html);
                  <?php
                  if(!isset($schedule_info) || empty($schedule_info)){
                  ?>
                $('#exam_schedule').html(sub_html);
                  <?php } ?>

            }
        });
    });
    <?PHP
if ($act == "update") {
    ?>
       $('#exam_class_id').change();
     <?php }?>
</script>
